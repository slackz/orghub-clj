(ns orghub.server
  (:gen-class)
  (:require [buddy.hashers :as hashers]
            [cheshire.core :refer [generate-string parse-string]]
            [compojure.core :refer [GET POST defroutes]]
            [compojure.route :as route]
            [orghub.db :as db]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.util.response :as resp]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery
                                                  *anti-forgery-token*]]
            [ring.middleware.session.cookie :refer [cookie-store]]
            [ring.middleware.session :refer [wrap-session]]))

(defn json-body [req]
  (parse-string (slurp (:body req)) true))

(defn login [login-info]
  (let [email  (:email login-info)
        pw     (:password login-info)
        user   (db/find-by :users :email email)]
    ;; check that user-provided password matches digest in db
    ;;   inverse of (hashers/derive (:password "secret"))
    (hashers/check pw (:password_digest user))))

(defroutes app-routes
  (POST "/login" req
        (println (:session req))
        (let [session (:session req {})
              authenticated? (login (json-body req))]
          (-> (resp/response (str authenticated?))
              (assoc-in [:session :authenticated?] authenticated?))))

  (GET "/csrf" req
       (println (:session req))
       (-> (resp/response *anti-forgery-token*)
           (assoc :session (:session req))))

  (route/not-found "Page not found")
  (route/resources "/"))

(def app
  (let [cookie-secret (System/getenv "COOKIE_SECRET")
        session-opts  {:store (cookie-store {:key cookie-secret})}]
    (-> app-routes
        wrap-anti-forgery
        (wrap-session session-opts))))

(defn -main [& args]
  (run-jetty app {:port 3000}))
